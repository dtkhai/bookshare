import scrapy

class QuotesSpider(scrapy.Spider):
    name = "quotes"
    count = 0
    start_urls = [
        'https://www.rottentomatoes.com/m/memento/reviews/',
    ]

    def parse(self, response):
        for quote in response.css('div.review_table_row'):
            self.count += 1
            if (quote.css('div.review_icon::attr(class)').extract_first() == 'review_icon icon small fresh'):
                rate = 1
            else:
                rate = 0
            yield {
                'id': self.count,
                'review': quote.css('div.the_review::text').extract_first(),                
                'rating': rate
            }

        next_page = response.css('a.btn-primary-rt:nth-child(n+2)::attr("href")').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
